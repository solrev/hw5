# -*- coding: utf-8 -*-
"""
Created on Thu Nov 20 16:37:02 2014

@author: Dave
"""

import socket
import sys

host = 'localhost'
port = 50000
size = 1024
s = None

while 1:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        s.connect((host,port))
    except socket.error, (value, message):
        if s:
            s.close()
        print "Could not open socket: " + message
        sys.exit(1)
        
    var = raw_input("--> ")
    s.send(var)
    data = s.recv(size)
    print "Received: ", data
    
    s.close()
    