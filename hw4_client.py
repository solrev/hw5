# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 12:14:22 2014
client window

This will take the screeen sent from gatekeeper and display it, it will also 
ping the gatekeeper with the event or keystroke from the client computer

@author: solrev
"""
import pygame


#import gatekeeper as gk
black = (0, 0, 0)
white = (255, 255, 255)
blue =  (0, 0, 255)
red = (255, 0, 0)
green = (0, 255, 0)
yellow = (255, 255, 0)
silver = (192, 192, 192)

done = False

pygame.init()

# Create an 800x600 sized screen
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode([screen_width, screen_height]) 
# Set the title of the window
pygame.display.set_caption("DAVE AND GREG'S CRUEL PONG") 
# Create a surface we can draw on
background = pygame.Surface(screen.get_size())
# Used for converting color maps and such
background = background.convert() 
# Fill the screen with a black background
background.fill(green)

all_sprites = pygame.sprite.Group()

key_down = {pygame.K_UP: "('player2', 'arrow_up','Down')", pygame.K_DOWN:"('player2', 'arrow_down','Down')",
            pygame.K_w: "('player1', 'w','Down')", pygame.K_s: "('player1', 's','Down')", 
            pygame.K_d: "('both', 'd','Down')", pygame.K_l: "('both', 'l','Down')",
            pygame.K_r: "('both', 'r','Down')", pygame.K_v: "('both', 'v','Down')"}

key_up = {pygame.K_UP: "('player2', 'arrow_up','Up')", pygame.K_DOWN:"('player2', 'arrow_down','Up')",
            pygame.K_w: "('player1', 'w','Up')", pygame.K_s: "('player1', 's','Up')", 
            pygame.K_d: "('both', 'd','Up')", pygame.K_l: "('both', 'l','Up')",
            pygame.K_r: "('both', 'r','Up')", pygame.K_v: "('both', 'v','Up')"}

stroke = "('no player', 'no key','no direction')"
            
def reset_stroke():
    global stroke
    stroke = "('no player', 'no key','no direction')"
    return stroke 
       
#stroke = [player, key, direction]
def sent_key():
    global stroke
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            pygame.quit()
        elif event.type == pygame.KEYDOWN:             
            stroke = key_down[event.key]               
        elif event.type == pygame.KEYUP:
            stroke = key_up[event.key]
        else:
            stroke = reset_stroke()

    
    return stroke
    #clock.tick(30)

def new_screen(all_sprites):
            
    screen.fill(green)     
    # Draw the sprites
    all_sprites.draw(screen) 
    # Display the screen
    pygame.display.flip()  
# All done, shut down Pygame            
