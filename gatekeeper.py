# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 11:44:26 2014

@author: solrev
This will act as the gate keeper program for Dave and Greg's Cruel pong
The idea is to use hw4_pong_game as the server file and this will distribute 
commands to the clients

This file will look for server pings and dish the appropriate keys to the server
The server will be blind to the actual keys, just the event they represent

It will return the current status of the board, including score, position, etc
But it will only be an inmage for the clients to display.  all the calculations 
will be internal to the server

get_keys()
    check for player key state from client pings
    return key, event(up or down)
    
Show_it()
    send the board to clients
    
Kill(player)
    send a quit command to client(player)
    
Kill_all()
    Kill(player1)
    Kill(player2)

#########
for now we will replace the code in server but use this file to talk to pygame
#########    
"""

import pygame 
import re
import hw4_client as h4c

done = False
pygame.init()
clock = pygame.time.Clock() 
loop = 0

def get_keys():
    global loop
    stroke = h4c.sent_key()
    
    up_look = re.search('Up',stroke)
    if up_look and loop == 2:
        h4c.reset_stroke()
        loop = 0
    
    loop += 1
    
    return stroke
#    print stroke
#              
#    clock.tick(30)

def show_it(all_sprites):
    h4c.new_screen(all_sprites)