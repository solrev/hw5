# -*- coding: utf-8 -*-
"""
Created on Thu Dec 11 18:36:33 2014

@author: solrev
"""
import re

def print_me(val):
    print val
    
stroke =  ["('player1','arrow_up','Down')","('player1','arrow_up','Up')"]

pr1 = lambda: print_me("pushed up")
pr2 = lambda: print_me("pushed down")

command = [pr1, pr2]

combo = zip(stroke,command)


sid = "('player1','arrow_up','Down')"


for index, keys in enumerate(stroke):

    match = re.search(sid,keys)
    
    if match:
        print "worked"
        command[index]()
