# -*- coding: utf-8 -*-
"""
Created on Tue Nov 18 15:52:27 2014

@author: solrev
"""
# open files with 'with'
#with open('file name') as variable:
    #read
import urllib

htmlfile=urllib.urlopen("https://google.com")

htmltext = htmlfile.read()

print " START OF PAGE\n"
print htmltext
print "I'm done!\n"